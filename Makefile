
# Debian-specific Makefile, see debian/copyright for licensing info
# Copyright (C): 2007, Stefano Zacchiroli <zack@debian.org>

DESTDIR =

all: META
	$(MAKE) -C yaml/ all
install:
	$(MAKE) -C yaml/ install DESTDIR=$(DESTDIR)
clean:
	rm -f META
	$(MAKE) -C yaml/ clean
META: META.in
	sed s/@VERSION@/$$DEB_UPSTREAM_VERSION/ < $< > $@

